# Intro
This is an adapter used to connect to the University of Pennsylvania's extranet, given a valid username and password. It returns an authenticated Mechanize agent, allowing the user to access any pages that an authenticated user could access.

# Usage
## Add to Gemfile
```
gem "penn-extranet-adapter"
```

## Create an instance of the PennExtranetAdapter object
```
p = PennExtranetAdapter.new( user, pw )
```

## Getting a page
These return a string containing the raw HTML
```
source = p.get( url, params )
source = p.post( url, params )
```

## Directly accessing the authenticated Mechanize object
```
o = p.authenticated_agent
```

## Storing the authentication credentials in a file to reduce individual logins
```
p.save_agent
p.load_agent
```